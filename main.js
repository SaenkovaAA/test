var array = [6, 2, 3, 1, 7, 4, 8, 5]
var newArray = [].concat(array);

const readMoreBtn = document.querySelector("button");

function ArrSort(arr) {
     if (arr.length <= 1) {
        return arr;
    }
     
    const pivot = arr[arr.length - 1];
    const leftArr = [];
    const rightArr = [];
  
    for (let i = 0; i < arr.length - 1; i++) {
           if (arr[i] < pivot) {
            leftArr.push(arr[i]);
        }
        else {
            rightArr.push(arr[i])
        }
    }
    return newArray = [...ArrSort(leftArr), pivot, ...ArrSort(rightArr)];
}

readMoreBtn.addEventListener("click", function(e) {
    e.preventDefault();
    if (btn.textContent === 'Сортировка!'){
        ArrSort(newArray);
        DrawArray();
        btn.textContent = 'Теперь обратно'
    }
    else{
        newArray = []
        newArray = array.slice();
        DrawArray()
        btn.textContent = 'Сортировка!'
    }
})
function DrawArray() {
    var wrapper = $(".blocks-wrapper");
    wrapper.html('');
    newArray.forEach(element => {
        wrapper.append(`<div class="block" id="block-${element}">${element}</div>`);
    });
    wrapper.addClass('reDrawed');

    setTimeout(() => {
        wrapper.removeClass('reDrawed');
    },1000)
}